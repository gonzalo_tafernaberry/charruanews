# -*- coding: utf-8 -*-
import cgi
import os
import re
import json
import webapp2
import jinja2
import traceback
import urllib, urllib2
import httplib
import time
import sys

from google.appengine.ext.webapp import template
from google.appengine.api import users
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext import ndb
import webapp2
import main
from webapp2_extras import sessions
from bs4 import BeautifulSoup

from models.models import News
from monkeylearn import Mk
from save import SaveNews


class LaDiaria2(webapp2.RequestHandler):
      def get(self):
            try:
                  name = "/articulo/2015/9/la-construccion-de-mas-igualdad/"
                  url = "http://elmonoinfoarmado.appspot.com/la_diaria?name=/articulo/2015/9/la-construccion-de-mas-igualdad/"
                  #url = "http://charruanews.appspot.com/la_diaria"
                  #url = url + "?name=/articulo/2015/9/la-construccion-de-mas-igualdad/"
                  sock = urllib2.urlopen(url)
                  htmlSource = (sock.read()).decode("utf-8")
                  data = json.loads(htmlSource)
                  sock.close()   
                  soup = BeautifulSoup(htmlSource, "html.parser")
                  data = (soup.find_all("article"))[0]
                  
                  template = main.JINJA_ENVIRONMENT.get_template('/templates/json.html')
                  self.response.write(template.render(locals()))
            except:
                  print traceback.format_exc()
                  return traceback.format_exc()

class LaDiaria(webapp2.RequestHandler):
      def hack_la_diaria(self, url):
            try:
                  name = "/articulo/2015/9/la-construccion-de-mas-igualdad/"
                  url = "http://elmonoinfoarmado.appspot.com/la_diaria?name=/articulo/2015/9/la-construccion-de-mas-igualdad/"
                  #url = "http://charruanews.appspot.com/la_diaria"
                  #url = url + "?name=/articulo/2015/9/la-construccion-de-mas-igualdad/"
                  sock = urllib2.urlopen(url)
                  htmlSource = (sock.read()).decode("utf-8")
                  data = json.loads(htmlSource)
                  sock.close()   
                  soup = BeautifulSoup(htmlSource, "html.parser")
                  data = soup.find_all("article")
                  
                  template = main.JINJA_ENVIRONMENT.get_template('/templates/json.html')
                  self.response.write(template.render(locals()))
            except:
                  print traceback.format_exc()
                  return traceback.format_exc()

      def get(self):
            try:
                  page = "http://ladiaria.com.uy/"
                  print "\n"
                  print "LaDiaria"
                  print "\n"
                  #time.sleep(2)
                  #page = "http://www.contragolpe.com.uy/articulo/57192_la-zancadilla-de-un-camarografo-a-usain-bolt"
                  #page = "http://www.180.com.uy/articulo/57260_consejo-de-ministros-respaldo-levantar-esencialidad"
                  sock = urllib2.urlopen(page)
                  htmlSource = sock.read()
                  sock.close()   
                  soup = BeautifulSoup(htmlSource, "html.parser")
                  url = soup.find_all("article")
                  c = 0; l = []
                  for i in url:
                        try:
                              url = (i.find_all("a"))[0]
                        except:
                              print i
                              return True
                        try:
                              img = (url.find_all("img"))[0]
                              src = img["src"]
                              if "http://ladiaria.com.uy" not in str(src):
                                    src = "http://ladiaria.com.uy" + src
                              image = "<img src='" + src + "'/>"
                        except:
                              image = ""

                        url = url["href"]
                        url = "http://ladiaria.com.uy" + url
                        
                        try:
                              title = (i.find_all("h2"))[0]
                              title = (title.find_all("a"))[0]
                        except:
                              try:
                                    title = (i.find_all("h1"))[0]
                                    title = (title.find_all("a"))[0]
                              except:
                                    print traceback.format_exc()
                                    return i
                        title = title.string
                        try:
                              subtitle = (i.find_all("p"))[0]
                              subtitle = subtitle.string
                        except:
                              print traceback.format_exc()
                              subtitle = ""
                        
                        e = SaveNews()
                        e = e.if_exist(title, "la_diaria")
                        if e:
                              print "EXIST"
                              continue
                        else: print "NO EXISTE"
                        try:
                              princ = Mk()
                              print "pre mk text"
                              print str(url)
                              text = princ.monkey_learn_text_extract(url)
                              #text = unicode(text)
                              print [text]
                              if not text:
                                    print url
                                    return True
                                    data5 = princ.my_text_extract(url)
                                    break

                              #break
                              #sys.exit()
                        except:
                              print traceback.format_exc()
                              continue
                        try:
                              princ = Mk()
                              print "mk keyword"
                              #print [text]
                              if text:
                                    keyword = princ.monkey_learn_keyword_extract(text)
                                    
                                    if not keyword:
                                          print "\n"
                                          print " no keyword"
                                          print "\n" 
                                          continue
                              else:
                                    print "\n"
                                    print " no keyword"
                                    print "\n"
                                    continue
                        except:
                              print traceback.format_exc()
                              continue
                        #return text
                        print "pre save"
                        try:
                              s = SaveNews()
                              sav = s.saveme(
                                    title = title,
                                    subtitle = subtitle,
                                    image = image,
                                    url = url,
                                    category = "",
                                    keyword = keyword,
                                    news_from = "la_diaria",
                                    id = c,
                                    html = text,
                              )

                              print "save"
                        except:
                              print traceback.format_exc()
                              continue
                        
                        #l.append(d)
                        print c
                        c = c + 1
                      
                  
                  return l
            except: 
                  print traceback.format_exc()
                  return traceback.format_exc()