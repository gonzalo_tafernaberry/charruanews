//$(window).load(function() { 
$(document).ready(function () {
    // will fire IMMEDIATELY after the DOM is constructed
    
    $(".clima-display").css( "display", "inline");

    $(window).load(function() {
        // will only fire AFTER all pages assets have loaded
        $("img").css( "display", "block"); 
    });

    $.ajax({
      url: "/on_load", 
      success: function(result){
        $("#load").html(result);
        $.ajax({
          url: "/on_load2", 
          success: function(result){
            $("#load2").html(result);
          },

        });
      },

    });
    // layout Masonry after each image loads
});

